package models;

public class HasilOperasi {
    private int resultTambah;
    private int resultKurang;
    private int resultBagi;
    private int resultKali;

    public int getResultTambah(){
        return resultTambah;
    }
    public int getResultKurang(){
        return resultKurang;
    }
    public int getResultBagi(){
        return resultBagi;
    }
    public int getResultKali(){
        return resultKali;
    }

    public int setHasilTambah(Integer resultTambah) {
        this.resultTambah = resultTambah;
        return resultTambah;
    }
    public int setHasilKurang(Integer resultKurang) {
        this.resultKurang = resultKurang;
        return resultKurang;
    }
    public int setHasilBagi(int resultBagi) {
        this.resultBagi = resultBagi;
        return resultBagi;
    }
    public int setHasilKali(int resultKali) {
        this.resultKali = resultKali;
        return resultKali;
    }
}
