package utils;

import models.HasilOperasi;
import ui.Activity;

import java.io.*;

import static utils.Constant.LOKASI_FILE;

public class ConvertFile {
    public void main(HasilOperasi hasilOperasi) throws IOException {
        BufferedWriter bwr;
        bwr = null;
        try {
            File fileLocation = new File(LOKASI_FILE);
            FileWriter writer = new FileWriter(fileLocation);
            bwr = new BufferedWriter(writer);
            if (fileLocation.createNewFile()){
                System.out.println("new file is created");
            }
            bwr.write("File Operasi");
            bwr.newLine();
            bwr.write("Hasil penjumlahan:" + hasilOperasi.getResultTambah());
            bwr.newLine();
            bwr.write("Hasil pengurangan:" + hasilOperasi.getResultKurang());
            bwr.newLine();
            bwr.write("Hasil perkalian:" + hasilOperasi.getResultKali());
            bwr.newLine();
            bwr.write("Hasil pembagian:" + hasilOperasi.getResultBagi());
            bwr.newLine();
            bwr.write("----created by Fathan");
            bwr.newLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            bwr.flush();
            bwr.close();
            System.out.println("Successfully written to a file");
            new Activity();
        }
    }
}
