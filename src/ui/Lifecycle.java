package ui;

import java.io.IOException;

public interface Lifecycle {
    void onStart() throws IOException;
    void onStop();
    void onDestroy();
}
