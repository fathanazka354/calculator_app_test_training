package ui;

import java.io.IOException;
import java.util.Scanner;

public abstract class BaseApp implements Lifecycle {
    private final Scanner scanner;
    private boolean isRunning;

    protected BaseApp() {
        isRunning = true;
        scanner = new Scanner(System.in);
    }

    abstract protected void create(Scanner scanner) throws IOException;

    protected BaseApp(Scanner scanner, boolean isRunning) {
        this.scanner = scanner;
        this.isRunning = isRunning;
    }

    public void onStart() throws IOException {
        while (isRunning){
            create(scanner);
        }
    }

    public void onDestroy(){
        scanner.close();
        System.exit(1);
    }

    public void onStop(){
        isRunning = false;
    }
}
