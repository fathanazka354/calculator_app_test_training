package ui;

import models.HasilOperasi;
import utils.ConvertFile;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Activity extends BaseApp{
    public Activity() throws IOException {
        onStart();
    }
    private HasilOperasi hasilOperasi = new HasilOperasi();

    @Override
    protected void create(Scanner scanner) throws IOException {
        System.out.println("-----------------------------");
        System.out.println("Program Calculator sederhana");
        System.out.println("-----------------------------");
        System.out.println();
        try {
            System.out.print("Masukkan angka A: ");
            int inputAngkaA = Integer.parseInt(scanner.nextLine());
            System.out.print("Masukkan angka B: ");
            int inputAngkaB = Integer.parseInt(scanner.nextLine());
            operasiHitung(inputAngkaA, inputAngkaB, scanner);
        }catch (InputMismatchException inputMismatchException){
            System.out.println("Input Angka dengan benar, hanya berlaku bilangan bulat!");
            new Activity();
        }
    }

    private void operasiHitung(int inputAngkaA, int inputAngkaB, Scanner scanner) throws IOException {
            System.out.println();
            System.out.println("-----------------------------");
            System.out.println("Pilih Operasi yang akan dilakukan");
            System.out.println("-----------------------------");
            System.out.println("1. tambah");
            System.out.println("2. kurang");
            System.out.println("3. kali");
            System.out.println("4. bagi");
            System.out.println("5. kembali input");
            System.out.println("6. convert file");
            System.out.println("0. keluar");
            System.out.println("tambahannya="+ hasilOperasi.getResultTambah());
            System.out.print("Input pilihan:");
            try {
                int pilihan = Integer.parseInt(scanner.nextLine());
                switch (pilihan){
                    case 1:
                        try {
                            int result = inputAngkaA + inputAngkaB;
                            System.out.print("Penjumlahan antara "+ inputAngkaA + " + "+ inputAngkaB +" = " + result);
                            hasilOperasi.setHasilTambah(result);
                        }catch (ArithmeticException arithmeticException){
                            System.out.println(arithmeticException);
                            messageError();
                        }finally {
                            operasiHitung(inputAngkaA,inputAngkaB,scanner);
                        }
                    case 2:
                        try {
                            var result = inputAngkaA - inputAngkaB;
                            System.out.print("Penjumlahan antara "+ inputAngkaA + " - "+ inputAngkaB +" = " + result);
                            hasilOperasi.setHasilKurang(result);
                        }catch (ArithmeticException arithmeticException){
                            System.out.println(arithmeticException);
                            messageError();
                        }finally {
                            operasiHitung(inputAngkaA,inputAngkaB,scanner);
                        }
                    case 3:
                        try {
                            var result = inputAngkaA / inputAngkaB;
                            System.out.print("Penjumlahan antara "+ inputAngkaA + " / "+ inputAngkaB +" = " + result);
                            hasilOperasi.setHasilBagi(result);
                        }catch (ArithmeticException arithmeticException){
                            System.out.println(arithmeticException);
                            messageError();
                        }catch (Exception exception){
                            System.out.println(exception);
                            messageError();
                        }finally {
                            operasiHitung(inputAngkaA,inputAngkaB,scanner);
                        }
                    case 4:
                        try {
                            var result = inputAngkaA * inputAngkaB;
                            System.out.print("Penjumlahan antara "+ inputAngkaA + " x "+ inputAngkaB +" = " + result);
                            hasilOperasi.setHasilKali(result);
                        }catch (ArithmeticException arithmeticException){
                            System.out.println(arithmeticException);
                            messageError();
                        }finally {
                            operasiHitung(inputAngkaA,inputAngkaB,scanner);
                        }
                    case 5:
                        new Activity();
                    case 6:
                        ConvertFile convertFile = new ConvertFile();
                        convertFile.main(hasilOperasi);
                    case 0:
                        onDestroy();
                    default:
                        System.out.println("Masukkan pilihan dengan benar!");
                        operasiHitung(inputAngkaA,inputAngkaB,scanner);
                }

            }catch (InputMismatchException inputMismatchException){
                System.out.println("Input Pilihan dengan angka yang benar!!");
                new Activity();
            }
    }
    private void messageError(){
        System.out.println("-----------------------------");
        System.out.println("Input angka dengan benar!");
        System.out.println("Angka Hanya berupa bilangan bulat!");
        System.out.println("-----------------------------");
    }
}
